Tibco BW vs Java: Speed Coding Challenge

The following challenge will once and for all determine which tool

1. Select all employees from EMPLOYEES table, who were Hired after 27 April 1994 and with a Salary above 3000. (2)

2. For each employee from the result set, construct the following schema:

ID: EMPLOYEE_ID (1)

FullName: FIRST_NAME +’ ‘+ LAST_NAME (2)

Username: Reverse FIRST_NAME + Random 3 digit number (3)

Email: EMAIL + ‘@oneconnect.co.za’ (2)

Region: Set to ‘NORTH’ if last 3 numbers of PHONE_NUMBER are between 000 and 249

Set to ‘SOUTH’ if last 3 numbers of PHONE_NUMBER are between 250 and 499

Set to ‘EAST’ if last 3 numbers of PHONE_NUMBER are between 500 and 749

Set to ‘WEST’ if last 3 numbers of PHONE_NUMBER are between 750 and 999

(4)

DaysToAnniversary: Calculate the number of days remaining until the next HIRE_DATE anniversary (3)

Profession: Remove ‘_’ from JOB_ID and set it to lowercase (2)

Pay: Convert SALARY to rands ( *15.407) and set to 2 decimal places (2)

3. Combine all data for all the employees into one string and send it to the FreedomFighters queue. (5)

TOTAL: 26