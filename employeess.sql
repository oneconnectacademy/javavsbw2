-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 13, 2019 at 02:45 PM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `employees`
--

-- --------------------------------------------------------

--
-- Table structure for table `emplyeess`
--

CREATE TABLE `emplyeess` (
  `EMPLOYEE_ID` bigint(7) NOT NULL,
  `FIRST_NAME` varchar(225) NOT NULL,
  `LAST_NAME` varchar(225) NOT NULL,
  `EMAIL` varchar(225) NOT NULL,
  `CONTACT_NO` varchar(225) NOT NULL,
  `HIRE_DATE` date NOT NULL,
  `JOB_ID` varchar(225) NOT NULL,
  `SALARY` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `emplyeess`
--

INSERT INTO `emplyeess` (`EMPLOYEE_ID`, `FIRST_NAME`, `LAST_NAME`, `EMAIL`, `CONTACT_NO`, `HIRE_DATE`, `JOB_ID`, `SALARY`) VALUES
(324561, 'Ramalepa', 'Katlego', 'Katlego@oneconnect.co.za', '0788431965', '1996-05-01', 'ONE_Clerk', 2500),
(324563, 'Ntwe', 'Crozier', 'crozier@oneconnect.co.za', '0603195114', '1994-05-01', 'ONE_Intern', 5500),
(324565, 'Sithole', 'Rogers', 'Rogers@oneconnect.co.za', '0718715265', '1992-02-20', 'Co-Founder', 6000000),
(324567, 'Lerutle', 'Sophia', 'sophia@oneconnect.co.za', '0794173173', '1994-05-01', 'ONE_Intern', 5500),
(324568, 'Masupye', 'Lesego', 'lesego@oneconnect.co.za', '0614365873', '1993-05-01', 'ONE_CFO', 120000);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `emplyeess`
--
ALTER TABLE `emplyeess`
  ADD PRIMARY KEY (`EMPLOYEE_ID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
